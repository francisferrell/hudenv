
.PHONY: release
release:
	@if [ -n "$(VERSION)" ]; then true; else echo "Error: VERSION not specified. If you're calling 'make release' directly, make sure you know what you're doing!"; exit 2; fi
	@if [ "$$( git rev-parse --abbrev-ref HEAD )" = 'master' ]; then true; else echo "Error: branch is '$$( git rev-parse --abbrev-ref HEAD )'; expected 'master'"; exit 2; fi
	@echo Releasing $(VERSION)!
	@echo
	git fetch origin
	git tag '$(VERSION)'
	git checkout ubuntu
	git reset --hard origin/ubuntu
	git merge master --no-edit --message 'merge master into ubuntu for release of $(VERSION)'
	gbp dch --new-version='$(VERSION)-1' --release --distribution=eoan --force-distribution --commit --commit-msg='release $(VERSION) for ubuntu' --spawn-editor=never
	git tag 'ubuntu/$(VERSION)'
	git checkout master
	@echo
	@echo Release workflow complete. Now run:
	@echo
	@echo '       git push origin ubuntu'
	@echo '       git push --tags'
	@echo



.PHONY: revbump minorbump majorbump
revbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1,$$2,$$3+1}' )"

minorbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1,$$2+1,0}' )"

majorbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1+1,0,0}' )"



venv/bin/hudlaunch: requirements.txt
	rm -rf venv
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt
	./venv/bin/pip install -r https://gitlab.com/francisferrell/dagr/-/raw/master/requirements.txt
	./venv/bin/pip install install git+https://gitlab.com/francisferrell/dagr.git@master
	./venv/bin/pip install -e .
	if [ -e /usr/lib/x86_64-linux-gnu/qt5/plugins/platformthemes/libqt5ct.so ] ; then \
		mkdir -p venv/lib/python3.7/site-packages/PySide2/Qt/plugins/platformthemes ; \
		ln -s /usr/lib/x86_64-linux-gnu/qt5/plugins/platformthemes/libqt5ct.so venv/lib/python3.7/site-packages/PySide2/Qt/plugins/platformthemes/ ; \
	fi
	if [ -e /usr/lib/x86_64-linux-gnu/qt5/plugins/styles/libqt5ct-style.so ] ; then \
		mkdir venv/lib/python3.7/site-packages/PySide2/Qt/plugins/styles ; \
		ln -s /usr/lib/x86_64-linux-gnu/qt5/plugins/styles/libqt5ct-style.so venv/lib/python3.7/site-packages/PySide2/Qt/plugins/styles/ ; \
	fi

hudlaunch: venv/bin/hudlaunch
	./venv/bin/hudlaunch

shell: venv/bin/hudlaunch
	./venv/bin/python -i -c "from PySide2.QtWidgets import QApplication; app = QApplication([])"



clean:
	rm -rf \
		hud*.egg-info/ \
		venv/
	find . -type d -name __pycache__ -prune -exec rm -rf {} \;

