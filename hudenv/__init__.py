
__title__ = 'hudenv'
__version__ = 'git'
__description__ = 'a heads up display for X'
__author__ = 'Francis Ferrell'
__author_email__ = 'francisferrell@gmail.com'
__copyright__ = 'Copyright 2020 {}'.format( __author__ )
__url__ = 'https://gitlab.com/francisferrell/hudenv'
__license__ = 'MIT'

