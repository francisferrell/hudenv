
import signal
from sys import argv

from PySide2.QtWidgets import QApplication

from .models import Launchable
from .utils.hotkey import HotkeyWatcher
from .views.launcher import LaunchView

from .utils.logger import getLogger
logger = getLogger( __name__ )



launch_view = None



def on_hotkey():
    logger.info( 'showing the LaunchView' )
    launch_view.show()



def on_activated( entry ):
    logger.info( 'launching: {!r}', entry )
    entry.launch()



def main():
    global launch_view

    app = QApplication( argv )
    app.setQuitOnLastWindowClosed( False )
    signal.signal( signal.SIGINT, signal.SIG_DFL )
    args = app.arguments()

    Launchable.initialize()
    launch_view = LaunchView()

    launch_view.entry_activated.connect( on_activated )

    hotkey_watcher = HotkeyWatcher()
    hotkey_watcher.start()
    hotkey_watcher.hotkey_pressed.connect( on_hotkey )

    return app.exec_()

