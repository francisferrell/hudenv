
from PySide2.QtCore import QProcess

from .launchable import Launchable
from ..utils import xdg

from ..utils.logger import getLogger
logger = getLogger( __name__ )



class Application( Launchable ):

    @classmethod
    def create_instances( cls ):
        xdg.parse_desktop_files()
        for app in xdg.desktop_applications:
            Application( app )


    def __init__( self, desktop_entry ):
        icon = desktop_entry.getIcon()
        if icon == '':
            icon = 'system-run'

        super().__init__( desktop_entry.getName(), icon )
        self.xdg_entry = desktop_entry


    def launch( self ):
        executable = self.xdg_entry.getExec().split( ' ' )[0]
        logger.debug( '{!r}: {!r} / {!r}', self, executable, self.xdg_entry.getTryExec() )

        process = QProcess()
        process.setProgram( executable )
        process.setStandardInputFile( QProcess.nullDevice() )
        process.setStandardOutputFile( QProcess.nullDevice() )
        process.setStandardErrorFile( QProcess.nullDevice() )
        if not process.startDetached():
            logger.error( 'failed to launch {!r}', executable )

