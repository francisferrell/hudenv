
from PySide2.QtGui import QIcon

from ..utils.logger import getLogger
logger = getLogger( __name__ )



class Launchable:

    instances = list()


    @classmethod
    def initialize( cls ):
        logger.debug( '{}.initialize', cls.__name__ )
        cls.create_instances()
        for sub_cls in cls.__subclasses__():
            sub_cls.initialize()


    @classmethod
    def create_instances( cls ):
        pass


    def __init__( self, name, icon ):
        self.name = name

        if isinstance( icon, str ):
            self.icon = QIcon.fromTheme( icon )
        else:
            self.icon = icon

        Launchable.instances.append( self )


    def __repr__( self ):
        return f'<{type(self).__name__} name={self.name!r}>'


    def launch( self ):
        pass

