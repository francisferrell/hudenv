
from .launchable import Launchable

# subclasses of Launchable must be imported here
# for Launchable.initialize to trickle down
from .application import Application
from .dagr import Dagr
from .system import SystemAction



import re

from PySide2.QtCore import (
    Qt,
    QAbstractListModel,
    QModelIndex,
    )

from ..utils.logger import getLogger
logger = getLogger( __name__ )



class LaunchableSearch( QAbstractListModel ):

    def __init__( self, parent = None ):
        super().__init__( parent )
        self._data = []


    def rowCount( self, parent = QModelIndex() ):
        return len( self._data )


    def data( self, index, role = Qt.DisplayRole ):
        if not index.isValid():
            logger.debug( 'ignoring invalid index: {!r}', index )
            return None

        try:
            entry = self._data[index.row()]
        except IndexError:
            logger.debug( 'ignoring out of bounds index: {!r}', index )
            return None

        if role == Qt.UserRole:
            return entry

        if role == Qt.DisplayRole:
            return entry.name

        if role == Qt.DecorationRole:
            return entry.icon

        return None


    def set_search_term( self, term ):
        self.beginResetModel()

        terms = [
            f'(^{term})',
            f'(\\b{term}\\b)',
            f'(\\b{term})',
            f'({term})',
        ]
        term_rx = f'(?:{ "|".join( terms ) })'
        logger.debug( f'matching with {term_rx}' )
        rx = re.compile( term_rx, re.IGNORECASE )
        matches = []

        for launchable in Launchable.instances:
            match = rx.search( launchable.name )
            if match is None:
                continue

            logger.debug( f'  {launchable.name} matches: {match.groups()!r}' )

            if match.group(1) is not None:
                matches.append( ( 1, launchable ) )
            elif match.group(2) is not None:
                matches.append( ( 2, launchable ) )
            elif match.group(3) is not None:
                matches.append( ( 3, launchable ) )
            elif match.group(4) is not None:
                matches.append( ( 4, launchable ) )
            else:
                raise ValueError( f"application matching shouldn't get here: term = {term!r}, launchable.name = {launchable.name!r}" )

        matches = sorted( matches, key = lambda m: ( m[0], m[1].name ) )
        self._data = [ m[1] for m in matches ]
        self.endResetModel()

