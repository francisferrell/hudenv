
from .launchable import Launchable
from ..utils import libdagr
from ..views.dagr import DagrView

from ..utils.logger import getLogger
logger = getLogger( __name__ )

dagr_view = None



class Dagr( Launchable ):

    @classmethod
    def create_instances( cls ):
        if not libdagr.installed:
            logger.debug( 'libdagr not found, skipping' )
            return

        Dagr()


    def __init__( self ):
        super().__init__( 'Dagr', 'system-run' )


    def launch( self ):
        global dagr_view
        if dagr_view is None or not dagr_view.isVisible():
            dagr_view = DagrView()
            dagr_view.show()

