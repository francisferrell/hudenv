
from os import environ
from time import time, sleep

from gi.repository import GLib
from pydbus import SystemBus

from .launchable import Launchable
from ..utils.message import error_dialog

from ..utils.logger import getLogger
logger = getLogger( __name__ )



# lxqt-lockscreen.desktop:Icon = system-lock-screen



class SystemAction( Launchable ):

    def __init__( self, name, icon ):
        super().__init__( name, icon )
        self.bus = SystemBus()



class LogindSimpleAction( SystemAction ):

    @classmethod
    def create_instances( cls ):
        LogindSimpleAction( 'Reboot', 'system-reboot', 'Reboot', False )
        LogindSimpleAction( 'Shutdown', 'system-shutdown', 'ScheduleShutdown', 'poweroff', 0 )


    def __init__( self, name, icon, method, *args ):
        super().__init__( name, icon )
        self.dbus_method = method
        self.dbus_args = args


    def launch( self ):
        try:
            manager = self.bus.get( 'org.freedesktop.login1', '/org/freedesktop/login1' )
            method = getattr( manager, self.dbus_method )
            method( *self.dbus_args )
        except GLib.GError as err:
            logger.error( 'GLib error while launching action {}: {}', self.name, err.message )
            error_dialog( 'DBus Error', err.message )
        except Exception as err:
            logger.exception( 'unhandled while launching action {}', self.name )
            error_dialog( 'Unhandled error', err )



class GreeterError( Exception ):
    pass



class LogoutAction( SystemAction ):

    @classmethod
    def create_instances( cls ):
        LogoutAction()


    def __init__( self ):
        super().__init__( 'Logout', 'system-log-out' )


    def activate_greeter( self ):
        manager = self.bus.get( 'org.freedesktop.login1', '/org/freedesktop/login1' )
        all_sessions = [ self.bus.get( 'org.freedesktop.login1', s[4] ) for s in manager.ListSessions() ]
        greeters = [ s for s in all_sessions if s.Class == 'greeter' ]

        if len( greeters ) == 0:
            logger.warn( "didn't find any greeter sessions. logout might not take you back to the greeter" )
            return
        elif len( greeters ) > 1:
            logger.warn( "found multiple greeter sessions, refusing to activate any of them" )
            return

        deadline = time() + 5.0
        greeters[0].Activate()

        while not greeters[0].Active:
            if time() > deadline:
                raise GreeterError( 'failed to activate greeter' )
            sleep( 0.1 )


    def launch( self ):
        try:
            user = self.bus.get( 'org.freedesktop.login1', '/org/freedesktop/login1/user/self' )
            sessions = [ self.bus.get( 'org.freedesktop.login1', s[1] ) for s in user.Sessions ]
            active_gui_sessions = [ s for s in sessions if s.Type == 'x11' and s.Active ]

            if len( active_gui_sessions ) == 0:
                raise ValueError( 'failed to find currently active login session' )
            elif len( active_gui_sessions ) > 1:
                raise ValueError( 'found multiple currently active login session, refusing to terminate any' )

            self.activate_greeter()
            active_gui_sessions[0].Terminate()

        except GreeterError as err:
            logger.error( 'GreeterError while launching action {}: {}', self.name, err )
            error_dialog( 'session logout error', err )
        except ValueError as err:
            logger.error( 'ValueError while launching action {}: {}', self.name, err )
            error_dialog( 'session logout error', err )
        except GLib.GError as err:
            logger.error( 'GLib error while launching action {}: {}', self.name, err.message )
            error_dialog( 'DBus Error', err.message )
        except Exception as err:
            logger.exception( 'unhandled while launching action {}', self.name )
            error_dialog( 'Unhandled error', err )

