
from PySide2.QtCore import Qt, QSize
from PySide2.QtWidgets import QLabel



class ElidedLabel( QLabel ):

    def __init__( self, text, parent = None ):
        super().__init__( text, parent )
        self._full_text = text


    def text( self ):
        return self._full_text


    def resizeEvent( self, event ):
        super().resizeEvent( event )
        font_metrics = self.fontMetrics()
        width = self.width() - 2
        clipped_text = font_metrics.elidedText( self._full_text, Qt.ElideRight, width )
        self.setText( clipped_text )
        if clipped_text != self._full_text:
            self.setToolTip( self._full_text )
        else:
            self.setToolTip( '' )


    def minimumSizeHint( self ):
        default = super().minimumSizeHint()
        return QSize( 0, default.height() )

