
from PySide2.QtCore import (
    Qt,
    Signal,
    )
from PySide2.QtWidgets import (
    QCompleter,
    QLineEdit,
    )

from ..utils.logger import getLogger
logger = getLogger( __name__ )



class CompletionEdit( QLineEdit ):

    completed = Signal( str )


    def __init__( self, choices, filter_mode = Qt.MatchStartsWith, case_sensitivity = Qt.CaseInsensitive, parent = None ):
        super().__init__( parent )

        choices = sorted( choices )
        completer = QCompleter( choices, self )
        completer.setCaseSensitivity( case_sensitivity )
        completer.setFilterMode( filter_mode )
        self.setCompleter( completer )

        completer.activated.connect( self.completed )
        self.textChanged.connect( self.reset_completion_prefix )


    def reset_completion_prefix( self, text ):
        if text == '':
            self.completer().setCompletionPrefix( '' )


    def keyPressEvent( self, event ):
        if event.key() == Qt.Key_Down:
            self.completer().complete()
        elif event.key() == Qt.Key_Tab:
            idx = self.completer().popup().currentIndex()
            new_idx = self.completer().model().index( idx.row() + 1 )
            self.completer().popup().setCurrentIndex( new_idx )
        else:
            super().keyPressEvent( event )


    def showEvent( self, event ):
        self.setText( '' )
        super().showEvent( event )

