
from PySide2.QtCore import (
    Qt,
    QSize,
    QModelIndex,
    Signal,
    )
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import (
    QApplication,
    QCompleter,
    QDialog,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QStyle,
    )

from .. import __title__ as app_title
from ..models import LaunchableSearch, Launchable

from ..utils.logger import getLogger
logger = getLogger( __name__ )



class LaunchCompleter( QCompleter ):

    entry_activated = Signal( Launchable )


    def __init__( self, parent ):
        super().__init__( parent )

        self.setCompletionRole( Qt.DisplayRole )
        self.setCompletionMode( QCompleter.UnfilteredPopupCompletion )
        self.popup().setIconSize( QSize( 48, 48 ) )

        self.setModel( LaunchableSearch( self ) )
        self.activated[QModelIndex].connect( self.activate_entry )


    def activate_entry( self, idx ):
        entry = self.model().data( idx, Qt.UserRole )
        self.entry_activated.emit( entry )


    def set_search_term( self, term ):
        self.model().set_search_term( term )



class SearchInput( QLineEdit ):

    def keyPressEvent( self, event ):
        if event.key() == Qt.Key_Down:
            self.completer().complete()
        elif event.key() == Qt.Key_Tab:
            idx = self.completer().popup().currentIndex()
            new_idx = self.completer().model().index( idx.row() + 1 )
            self.completer().popup().setCurrentIndex( new_idx )
        else:
            super().keyPressEvent( event )


    def showEvent( self, event ):
        self.setText( '' )
        super().showEvent( event )



class LaunchView( QDialog ):

    entry_activated = Signal( Launchable )


    def __init__( self, parent = None ):
        super().__init__( parent )
        self.setWindowTitle( app_title )

        layout = QHBoxLayout( self )

        icon = QIcon.fromTheme( 'system-run' )
        icon_label = QLabel()
        icon_label.setPixmap( icon.pixmap( 48 ) )
        layout.addWidget( icon_label, stretch = 0 )

        self.search = SearchInput()
        layout.addWidget( self.search, stretch = 1 )

        self.completer = LaunchCompleter( self )
        self.completer.entry_activated.connect( self.on_complete )
        self.search.setCompleter( self.completer )

        self.search.textEdited.connect( self.completer.set_search_term )


    def on_complete( self, entry ):
        self.hide()
        self.entry_activated.emit( entry )


    def showEvent( self, *args ):
        super().showEvent( *args )

        screen_geometry = QApplication.desktop().availableGeometry()
        size = self.size()
        size.setWidth( screen_geometry.width() * 0.33 )

        self.setGeometry(
            QStyle.alignedRect(
                Qt.LeftToRight,
                Qt.AlignCenter,
                size,
                screen_geometry
            )
        )

