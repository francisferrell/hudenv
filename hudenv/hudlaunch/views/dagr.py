
from datetime import date, datetime, timedelta
from functools import partial
from itertools import count

from PySide2.QtCore import (
    Qt,
    Signal,
    QSize,
    QTimer,
    )
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import (
    QApplication,
    QCheckBox,
    QDialog,
    QFrame,
    QGridLayout,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QStyle,
    QVBoxLayout,
    )

from .. import __title__ as app_title
from ..utils import libdagr
from ..utils.message import error_dialog
from ..utils.time import timedelta_to_expression
from ..widgets import CompletionEdit, ElidedLabel

from ..utils.logger import getLogger
logger = getLogger( __name__ )


COMMANDS = [ 'start', 'end', 'upload' ]
DAYS = [ '@today', '@yesterday' ]



class Report( QFrame ):

    deleted = Signal( datetime )


    def __init__( self, date, parent = None ):
        super().__init__( parent )
        self.date = date
        tasks = libdagr.get_tasks_in_thread( date = self.date )

        if len( tasks ) == 0:
            layout = QVBoxLayout( self )
            message = QLabel()
            message.setText( 'no entries' )
            message.setStyleSheet( 'font: italic;' )
            layout.addWidget( message )
            return

        layout = QGridLayout( self )
        col = count()
        layout.setHorizontalSpacing( 20 )
        layout.setColumnStretch( next(col), 0 )
        layout.setColumnStretch( next(col), 0 )
        layout.setColumnStretch( next(col), 0 )
        layout.setColumnStretch( next(col), 0 )
        layout.setColumnStretch( next(col), 1 )
        layout.setColumnStretch( next(col), 0 )

        icon_delete = QIcon.fromTheme( 'edit-delete-symbolic' )
        icon_reopen = QIcon.fromTheme( 'edit-undo' )
        icon_locked = QIcon.fromTheme( 'changes-prevent-symbolic' )
        icon_uploading = QIcon.fromTheme( 'emblem-synchronizing-symbolic' )
        icon_upload_done = QIcon.fromTheme( 'emblem-ok-symbolic' )
        icon_upload_error = QIcon.fromTheme( 'emblem-important-symbolic' )

        self.status_icons = dict()
        self.action_buttons = dict()

        row = 0
        day_duration = timedelta( seconds = 0 )
        for task in tasks:
            start = task.start.strftime( '%H:%M' )
            end = '' if task.isopen else task.end.strftime( '%H:%M' )
            delta = timedelta_to_expression( task.duration, delimiter = ' ' )
            day_duration += task.duration

            col = count()
            layout.addWidget( QLabel( start ), row, next(col) )
            layout.addWidget( QLabel( end ), row, next(col) )
            layout.addWidget( QLabel( delta ), row, next(col) )
            layout.addWidget( QLabel( task.ticket ), row, next(col) )
            layout.addWidget( ElidedLabel( task.description ), row, next(col) )

            action_button = QPushButton()
            action_button.setFlat( True )
            action_button.setFocusPolicy( Qt.NoFocus )
            action_button.setIconSize( QSize( 16, 16 ) )

            if task.isuploaded:
                action_button.setIcon( icon_locked )
            elif task.isterminal:
                action_button.setIcon( icon_reopen )
                action_button.clicked.connect( partial( self.deleted.emit, task.end ) )
            else:
                action_button.setIcon( icon_delete )
                action_button.clicked.connect( partial( self.deleted.emit, task.start ) )

            self.action_buttons[start] = action_button

            self.status_icons[start] = dict()
            status_uploading = QPushButton()
            status_uploading.setFlat( True )
            status_uploading.setFocusPolicy( Qt.NoFocus )
            status_uploading.setIconSize( QSize( 16, 16 ) )
            status_uploading.setIcon( icon_uploading )
            self.status_icons[start]['uploading'] = status_uploading

            status_upload_done = QPushButton()
            status_upload_done.setFlat( True )
            status_upload_done.setFocusPolicy( Qt.NoFocus )
            status_upload_done.setIconSize( QSize( 16, 16 ) )
            status_upload_done.setIcon( icon_upload_done )
            self.status_icons[start]['upload_done'] = status_upload_done

            status_upload_error = QPushButton()
            status_upload_error.setFlat( True )
            status_upload_error.setFocusPolicy( Qt.NoFocus )
            status_upload_error.setIconSize( QSize( 16, 16 ) )
            status_upload_error.setIcon( icon_upload_error )
            self.status_icons[start]['upload_error'] = status_upload_error

            status_col = next(col)
            layout.addWidget( action_button, row, status_col )
            layout.addWidget( status_uploading, row, status_col )
            layout.addWidget( status_upload_done, row, status_col )
            layout.addWidget( status_upload_error, row, status_col )
            status_uploading.hide()
            status_upload_done.hide()
            status_upload_error.hide()
            row += 1

        day_start = tasks[0].start.strftime( '%H:%M' )
        day_end = tasks[-1].end.strftime( '%H:%M' )
        day_delta = timedelta_to_expression( day_duration, delimiter = ' ' )

        col = count()
        for text in ( day_start, day_end, day_delta ):
            label = QLabel( text )
            label.setStyleSheet( 'font-weight: bold' )
            layout.addWidget( label, row, next(col) )



class BaseForm( QFrame ):

    report_changed = Signal()
    finished = Signal()


    def __init__( self, date, parent = None ):
        super().__init__( parent )
        self.date = date
        layout = QVBoxLayout( self )

        self.report = Report( self.date )
        self.report.deleted.connect( self.on_delete )
        layout.addWidget( self.report )

        self.input_area = QHBoxLayout()
        self.input_area.setContentsMargins( 0, 0, 0, 0 )
        layout.addLayout( self.input_area )


    def on_delete( self, time ):
        logger.info( 'on_delete( {!r} )', time )

        try:
            self.setDisabled( True )
            libdagr.delete_in_thread( datetime = time )
        except ( ValueError, libdagr.DagrError, libdagr.JIRAError ) as err:
            error_dialog( 'dagr: failed to delete', err )
        else:
            self.rebuild_report()
        finally:
            self.setDisabled( False )


    def rebuild_report( self ):
        old_report = self.report
        old_report.deleteLater()

        self.report = Report( self.date )
        self.report.deleted.connect( self.on_delete )

        self.layout().replaceWidget( old_report, self.report )
        self.report_changed.emit()



class CommandForm( BaseForm ):

    command_selected = Signal( str )


    def __init__( self, date, parent = None ):
        super().__init__( date, parent )

        self.edit = CompletionEdit( COMMANDS + DAYS )
        self.edit.setPlaceholderText( 'command' )
        self.edit.completed.connect( self.command_selected )
        self.edit.returnPressed.connect( self.select_command )

        self.setFocusProxy( self.edit )
        self.input_area.addWidget( self.edit )


    def select_command( self ):
        self.command_selected.emit( self.edit.text() )



class StartForm( BaseForm ):

    def __init__( self, date, parent = None ):
        super().__init__( date, parent )
        self.start_time = QLineEdit()
        self.start_time.setPlaceholderText( datetime.now().strftime( '%H:%M' ) )
        self.start_time.returnPressed.connect( self.start )

        today = libdagr.get_tasks_in_thread( date = self.date )
        for i in range( 1, 11 ):
            yesterday = libdagr.get_tasks_in_thread( dateref = '-{}d'.format( i ) )
            if len( yesterday ) > 0:
                break

        choices = [ ' '.join( [ t.ticket, t.description ] ).strip() for t in today + yesterday ]
        choices = sorted( list( set( choices ) ) )

        self.description = CompletionEdit( choices, filter_mode = Qt.MatchContains )
        self.description.textEdited.connect( self.slurp_start_time )
        self.description.completed.connect( self.start )
        self.description.returnPressed.connect( self.start )
        self.setFocusProxy( self.description )

        self.input_area.addWidget( self.start_time, 1 )
        self.input_area.addWidget( self.description, 3 )


    def slurp_start_time( self, text ):
        if self.start_time.text() == '' and ' ' in text:
            words = text.split()
            if libdagr.looks_like_timeref( words[0] ):
                self.start_time.setText( words[0] )
                self.description.setText( ' '.join( words[1:] ) )


    def start( self ):
        if self.description.text().strip() == '':
            return

        start_time = None
        ticket = None
        description = None

        if self.start_time.text().strip() != '':
            try:
                start_time = libdagr.timeref_to_datetime( self.start_time.text(), relative_to = self.date )
            except libdagr.DagrError as err:
                error_dialog( 'dagr: failed to start', err )
                return

        words = self.description.text().split()
        if libdagr.looks_like_ticket( words[0] ):
            ticket = words[0]
            description = ' '.join( words[1:] )
        else:
            description = self.description.text()

        logger.info( 'start( {!r}, {!r}, {!r} )', start_time, ticket, description )

        try:
            self.setDisabled( True )
            libdagr.start_in_thread(
                datetime = start_time,
                ticket = ticket,
                description = description
            )
        except libdagr.ConfigError as err:
            error_dialog( 'dagr: failed to start', 'error in configuration file {}: {}', err.config.path, err )
        except ( ValueError, libdagr.DagrError, libdagr.JIRAError ) as err:
            error_dialog( 'dagr: failed to start', err )
        else:
            self.finished.emit()
        finally:
            self.setDisabled( False )



class EndForm( BaseForm ):

    def __init__( self, date, parent = None ):
        super().__init__( date, parent )
        self.end_time = QLineEdit()
        self.end_time.setPlaceholderText( datetime.now().strftime( '%H:%M' ) )
        self.end_time.returnPressed.connect( self.end )
        placeholder = QLabel()
        self.setFocusProxy( self.end_time )
        self.input_area.addWidget( self.end_time, 1 )
        self.input_area.addWidget( placeholder, 3 )


    def end( self ):
        end_time = None

        if self.end_time.text().strip() != '':
            try:
                end_time = libdagr.timeref_to_datetime( self.end_time.text(), relative_to = self.date )
            except libdagr.DagrError as err:
                error_dialog( 'dagr: failed to end', err )
                return

        logger.info( 'end( {!r} )', end_time )

        try:
            self.setDisabled( True )
            libdagr.end_in_thread( datetime = end_time )
        except ( ValueError, libdagr.DagrError, libdagr.JIRAError ) as err:
            error_dialog( 'dagr: failed to end', err )
        else:
            self.finished.emit()
        finally:
            self.setDisabled( False )



class UploadForm( BaseForm ):

    def __init__( self, date, parent = None ):
        super().__init__( date, parent )

        self.force_check = QCheckBox( 'force' )

        upload_button = QPushButton( 'Upload' )
        upload_button.clicked.connect( self.upload )
        self.setFocusProxy( upload_button )

        self.input_area.addWidget( self.force_check, 3 )
        self.input_area.addWidget( upload_button, 1 )


    def upload( self ):
        self.upload_errors = False
        force = self.force_check.isChecked()
        logger.info( 'upload( force={!r} )', force )

        try:
            self.setDisabled( True )
            libdagr.upload_in_thread(
                date = self.date,
                force = force,
                before = self.before,
                after = self.after,
            )
        except ( ValueError, libdagr.DagrError, libdagr.JIRAError ) as err:
            error_dialog( 'dagr: failed to upload', err )
        else:
            if self.upload_errors:
                error_dialog( 'dagr: failed to upload', 'There were errors encountered while uploading!' )
            else:
                self.finished.emit()
        finally:
            self.setDisabled( False )


    def before( self, task ):
        logger.debug( 'before upload: {!r}', task )
        start = task.start.strftime( '%H:%M' )

        self.report.action_buttons[start].hide()
        self.report.status_icons[start]['uploading'].show()
        self.report.status_icons[start]['upload_done'].hide()
        self.report.status_icons[start]['upload_error'].hide()


    def after( self, task, err ):
        logger.debug( 'after upload: {!r}, {!r}', task, err )
        start = task.start.strftime( '%H:%M' )

        self.report.action_buttons[start].hide()
        self.report.status_icons[start]['uploading'].hide()
        if err is None:
            self.report.status_icons[start]['upload_done'].show()
            self.report.status_icons[start]['upload_error'].hide()
        else:
            self.upload_errors = True
            self.report.status_icons[start]['upload_done'].hide()
            self.report.status_icons[start]['upload_error'].setToolTip( str( err ) )
            self.report.status_icons[start]['upload_error'].show()



class DagrView( QDialog ):

    def __init__( self, parent = None ):
        if not libdagr.installed:
            raise RuntimeError( 'libdagr not installed' )

        super().__init__( parent )
        self.setWindowTitle( app_title + ' dagr' )
        QVBoxLayout( self )
        self.command = None
        self.date = date.today()
        self.build_ui()


    def build_ui( self ):
        if self.date == date.today():
            date_header = QLabel( 'today' )
        else:
            date_header = QLabel( self.date.strftime( '%a %b %-d %Y' ) )
        date_header.setStyleSheet( 'font-weight: bold;' )

        form = None

        if self.command == 'start':
            form = StartForm( self.date )
        elif self.command == 'end':
            form = EndForm( self.date )
        elif self.command == 'upload':
            form = UploadForm( self.date )
        else:
            form = CommandForm( self.date )
            form.command_selected.connect( self.on_command )

        form.finished.connect( self.reset_ui )
        form.report_changed.connect( self.adjust_window_size, type = Qt.QueuedConnection )
        self.layout().addWidget( date_header )
        self.layout().addWidget( form )
        form.setFocus()
        QTimer.singleShot( 0, self.adjust_window_size )


    def adjust_window_size( self ):
        new_size = self.sizeHint()
        new_size.setWidth( self.width() )
        logger.debug( 'resizing dialog to {}x{}', new_size.width(), new_size.height() )
        self.resize( new_size )


    def rebuild_ui( self ):
        for i in reversed( range( self.layout().count() ) ):
            self.layout().itemAt(i).widget().deleteLater()

        self.build_ui()


    def reset_ui( self ):
        self.command = None
        self.rebuild_ui()


    def on_command( self, command ):
        logger.debug( 'on_command( {!r} )', command )

        if command in COMMANDS:
            self.command = command
            self.rebuild_ui()
        elif libdagr.looks_like_dateref( command ):
            try:
                self.date = libdagr.dateref_to_date( command )
            except libdagr.InvalidDateref as err:
                error_dialog( 'dagr: failed to change days', err )
            else:
                self.rebuild_ui()
        else:
            logger.warn( 'unknown dagr command: {!r}', command )
            return


    def showEvent( self, *args ):
        super().showEvent( *args )

        screen_geometry = QApplication.desktop().availableGeometry()
        size = self.size()
        size.setWidth( screen_geometry.width() * 0.4 )

        self.setGeometry(
            QStyle.alignedRect(
                Qt.LeftToRight,
                Qt.AlignCenter,
                size,
                screen_geometry
            )
        )

