
try:
    from dagr import libdagr
except ImportError:
    libdagr = None
    installed = False
else:
    installed = True

from queue import Queue, Empty
from threading import Thread

from PySide2.QtCore import (
    Signal,
    QThread,
    )
from PySide2.QtWidgets import QApplication

from ..utils.logger import getLogger
logger = getLogger( __name__ )



THREADED_METHODS = {
    'get_tasks',
    'start',
    'end',
    'delete',
    'upload',
}



if installed:
    for name in dir( libdagr ):
        if name not in THREADED_METHODS:
            vars()[name] = getattr( libdagr, name )



def threaded_request( request, *args, **kwargs ):
    worker = DagrWorker( request, *args, **kwargs )
    worker.finished.connect( worker.deleteLater )
    worker.start()

    while not worker.wait( 50 ):
        QApplication.processEvents()

    if isinstance( worker.result, Exception ):
        raise worker.result
    else:
        return worker.result



def get_tasks_in_thread( *args, **kwargs ):
    return threaded_request( 'get_tasks', *args, **kwargs )



def start_in_thread( *args, **kwargs ):
    return threaded_request( 'start', *args, **kwargs )



def end_in_thread( *args, **kwargs ):
    return threaded_request( 'end', *args, **kwargs )



def delete_in_thread( *args, **kwargs ):
    return threaded_request( 'delete', *args, **kwargs )



def upload_in_thread( *args, **kwargs ):
    worker = DagrWorker( 'upload', *args, **kwargs )

    try:
        before_upload = kwargs.pop( 'before' )
    except KeyError:
        pass
    else:
        worker.before_upload.connect( before_upload )

    try:
        after_upload = kwargs.pop( 'after' )
    except KeyError:
        pass
    else:
        worker.after_upload.connect( after_upload )

    worker.finished.connect( worker.deleteLater )
    worker.start()

    while not worker.wait( 50 ):
        QApplication.processEvents()

    if isinstance( worker.result, Exception ):
        raise worker.result
    else:
        return worker.result



def mock_upload( date = None, force = False, before = None, after = None, **kwargs ):
    tasks = libdagr.get_tasks( date = date )
    for task in tasks:
        if task.ticket is None or task.ticket == '':
            continue

        if task.isuploaded and not force:
            continue

        before( task )
        QThread.sleep( 2 )
        if task.description == 'cause an upload error':
            after( task, ValueError( 'this is a mock error!' ) )
        else:
            after( task, None )



class DagrWorker( QThread ):

    before_upload = Signal( object )
    after_upload = Signal( object, object )

    def __init__( self, request, *args, **kwargs ):
        if not installed:
            raise RuntimeError( 'libdagr not installed' )

        super().__init__( None )
        self.request = request
        self.args = args
        self.kwargs = kwargs
        self.result = None


    def run( self ):
        logger.debug( 'starting threaded request={!r} args={!r} kwargs={!r}', self.request, self.args, self.kwargs )
        method = None

        if self.request == 'get_tasks':
            method = libdagr.get_tasks
        elif self.request == 'start':
            method = libdagr.start
        elif self.request == 'end':
            method = libdagr.end
        elif self.request == 'delete':
            method = libdagr.delete
        elif self.request == 'upload':
            method = libdagr.upload
            #method = mock_upload
            self.kwargs['before'] = self.before_upload.emit
            self.kwargs['after'] = self.after_upload.emit

        try:
            self.result = method( *self.args, **self.kwargs )
        except Exception as err:
            logger.debug( 'returning threaded error' )
            self.result = err
        else:
            logger.debug( 'returning threaded result' )

