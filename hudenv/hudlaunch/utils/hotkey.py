
from PySide2.QtCore import (
    Qt,
    QThread,
    Signal,
    )
import xcffib
from xcffib import xproto
from xcffib.xproto import KeyReleaseEvent

from ..utils.logger import getLogger
logger = getLogger( __name__ )



_xcb = None
_x_setup = None
_keymap = None



def keycode_for_keysym( sym ):
    global _keymap
    if _keymap is None:
        # taken from libqtile/core/xcbq.py refresh_keymap()
        first = _x_setup.min_keycode
        count = _x_setup.max_keycode - _x_setup.min_keycode + 1

        q = _xcb.core.GetKeyboardMapping( first, count ).reply()
        code_to_syms = dict()
        for i in range( len( q.keysyms ) // q.keysyms_per_keycode ):
            code_to_syms[first + i] = q.keysyms[i * q.keysyms_per_keycode:(i + 1) * q.keysyms_per_keycode]

        first_sym_to_code = dict()
        for k, s in code_to_syms.items():
            if s[0] and not s[0] in first_sym_to_code:
                first_sym_to_code[s[0]] = k

        _keymap = first_sym_to_code

    return _keymap.get( sym, 0 )



class HotkeyWatcher( QThread ):

    hotkey_pressed = Signal()


    def run( self ):
        global _xcb
        global _x_setup

        _xcb = xcffib.connect()
        _x_setup = _xcb.get_setup()

        self.bind_hotkey()

        logger.debug( 'entering xcb event loop' )
        while True:
            try:
                event = _xcb.wait_for_event()
            except xcffib.ProtocolException:
                logger.exception( "xcb protocol error!" )
            except:
                logger.exception( "unexpected error from xcb!" )
            else:
                logger.debug( 'received event: {!r}', event )
                if isinstance( event, KeyReleaseEvent ):
                    self.hotkey_pressed.emit()


    def bind_hotkey( self ):
        root = _x_setup.roots[0].root
        keycode = keycode_for_keysym( Qt.Key_Space )
        modifier = xproto.ModMask._4

        logger.debug( 'bindnig Super + space hotkey' )
        value = _xcb.core.GrabKey( 1, root, modifier, keycode, xproto.GrabMode.Async, xproto.GrabMode.Async, is_checked = True )
        # plus all combinations including CapsLock (Lock) and NumLock (_2)
        value = _xcb.core.GrabKey( 1, root, modifier | xproto.ModMask._2, keycode, xproto.GrabMode.Async, xproto.GrabMode.Async, is_checked = True )
        value = _xcb.core.GrabKey( 1, root, modifier | xproto.ModMask.Lock, keycode, xproto.GrabMode.Async, xproto.GrabMode.Async, is_checked = True )
        value = _xcb.core.GrabKey( 1, root, modifier | xproto.ModMask._2 | xproto.ModMask.Lock, keycode, xproto.GrabMode.Async, xproto.GrabMode.Async, is_checked = True )
        _xcb.flush()

