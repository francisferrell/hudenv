
from PySide2.QtWidgets import QMessageBox

from .. import __title__ as app_title



def error_dialog( title, message, *args, **kwargs ):
    if not isinstance( message, str ):
        message = str( message )
    if len( args ) > 0 or len( kwargs ) > 0:
        message = message.format( *args, **kwargs )
    QMessageBox.critical( None, '{}: {}'.format( app_title, title ), message )

