
from pathlib import Path

from xdg import BaseDirectory
from xdg.DesktopEntry import DesktopEntry
from xdg.Exceptions import ParsingError

from ..utils.logger import getLogger
logger = getLogger( __name__ )



desktop_applications = []
"""
list of xdg.DesktopEntry.DesktopEntry

All of the desktop applications that were found by parse_desktop_files.
"""



def parse_desktop_files():
    """
    Clear desktop_applications and recreate it from the .desktop files found on disk.

    This method searches all XDG data directories for .desktop files representing applications,
    filters out those that should be excluded ("Hidden" and "NoDisplay"), putting what remains
    in desktop_applications.
    """
    global desktop_applications
    desktop_applications = []

    for xdg_data_dir in BaseDirectory.xdg_data_dirs:
        xdg_apps_dir = Path( xdg_data_dir ) / 'applications'

        if not xdg_apps_dir.exists():
            logger.debug( 'ignoring non-existant {}', xdg_apps_dir )
            continue

        logger.debug( 'examining {}', xdg_apps_dir )
        for path in xdg_apps_dir.glob( '**/*.desktop' ):
            if path.parent.name == 'screensavers':
                logger.debug( 'ignoring screensaver {}', xdg_apps_dir )
                continue

            try:
                app = DesktopEntry( path )
            except ParsingError as err:
                logger.warning( '{}, skipping', err )
                continue

            app_name = app.getName()

            if app.getType() != 'Application':
                logger.debug( 'ignoring {!r} because: unknown Type {!r}', app_name, app.getType() )
                continue

            # NoDisplay means "this application exists, but don't display it in the menus"
            if app.getNoDisplay():
                logger.debug( 'ignoring {!r} because: NoDisplay', app_name )
                continue

            if app.getHidden():
                logger.debug( 'ignoring {!r} because: Hidden', app_name )
                continue

            if len( app.getOnlyShowIn() ) > 0:
                logger.debug( 'ignoring {!r} because: OnlyShowIn', app_name )
                continue

            logger.debug( 'found {!r}', app_name )
            desktop_applications.append( app )

    logger.info( 'found {!r} applications', len( desktop_applications ) )

