
import logging
from os import environ



root = logging.getLogger()

handler = logging.StreamHandler()
handler.setFormatter( logging.Formatter( '{asctime} {levelname:8s} {name}: {message}', style = '{' ) )
root.addHandler( handler )



class Message:

    def __init__( self, message, args, kwargs ):
        self.message = message
        self.args = args
        self.kwargs = kwargs

    def __str__( self ):
        return self.message.format( *self.args, **self.kwargs )



class StyleAdapter( logging.LoggerAdapter ):

    def __init__( self, logger, extra = None ):
        super().__init__( logger, extra or {} )

    def log( self, level, msg, *args, exc_info = None, stack_info = False, extra = None, **kwargs ):
        format_kwargs = kwargs
        logging_kwargs = dict( exc_info = exc_info, stack_info = stack_info, extra = extra )

        if self.isEnabledFor( level ):
            msg, logging_kwargs = self.process( msg, logging_kwargs )
            self.logger._log( level, Message( msg, args, format_kwargs ), (), **logging_kwargs )



if __package__ != '':
    _pkg_prefix = __package__.split( '.' )[0] + '.'
else:
    _pkg_prefix = None

def getLogger( name ):
    logger = logging.getLogger( name )
    if _pkg_prefix is not None and name.startswith( _pkg_prefix ) and len( name ) > len( _pkg_prefix ):
        short_name = name[len( _pkg_prefix):]
    else:
        short_name = name
    override = 'LOG_' + short_name.replace( '.', '_' ).upper()

    if override in environ:
        logger.setLevel( getattr( logging, environ[override].upper() ) )

    return StyleAdapter( logger )



if 'LOG' in environ:
    root.setLevel( getattr( logging, environ['LOG'].upper() ) )

