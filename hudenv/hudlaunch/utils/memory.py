
from os import getpid

from psutil import Process

PID = getpid()

__all__ = [ 'memory_usage', 'KB', 'MB' ]

B = 1
KB = 1024
MB = KB * 1024



def memory_usage( unit = KB ):
    proc = Process( PID )
    return proc.memory_info().rss / unit

