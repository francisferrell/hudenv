
ONE_MINUTE = 60
ONE_HOUR = ONE_MINUTE * 60
ONE_DAY = ONE_HOUR * 24



def timedelta_to_expression( delta, delimiter = '' ):
    """
    Convert a ``datetime.timedelta`` to a string expression, e.g. ``"4h12m"``.

    Args:
        delta (datetime.timedelta): the timedelta to convert
        delimiter (str): the delimiter between parts; defaults to the empty string, resulting
            in no delimiter.

    Returns:
        str: the timedelta expression
    """
    seconds = int( delta.total_seconds() )
    if seconds == 0:
        return '0s'

    parts = []

    if seconds >= ONE_DAY:
        parts.append( '{}d'.format( seconds // ONE_DAY ) )
        seconds = seconds % ONE_DAY

    if seconds >= ONE_HOUR:
        parts.append( '{}h'.format( seconds // ONE_HOUR ) )
        seconds = seconds % ONE_HOUR

    if seconds >= ONE_MINUTE:
        parts.append( '{}m'.format( seconds // ONE_MINUTE ) )
        seconds = seconds % ONE_MINUTE

    if seconds > 0:
        parts.append( '{}s'.format( seconds ) )

    return delimiter.join( parts )

