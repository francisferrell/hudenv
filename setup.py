
from setuptools import setup, find_packages

import os
import sys

sys.path.insert( 0, os.path.abspath( '.' ) )
import hudenv


setup(
    name = hudenv.__title__,
    version = '0.0' if hudenv.__version__ == 'git' else hudenv.__version__,
    description = hudenv.__description__,
    author = hudenv.__author__,
    author_email = hudenv.__author_email__,
    url = hudenv.__url__,
    license = hudenv.__license__,

    packages = find_packages(),

    entry_points = {
        'console_scripts': [
            'hudlaunch = hudenv.hudlaunch.main:main',
        ],
    }
)

